package com.rest.wapp.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rest.wapp.pojo.WeatherResponse;
import com.rest.wapp.service.WeatherService;

@Service
public class WeatherServiceImpl implements WeatherService {
	@Autowired
	public RestTemplate restTemplate;
	private static String APPID="dd3fc502c6b3a0e7901070312a016d6d";
	private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
	public WeatherResponse getWeather(String city) {
		System.out.println("Weather service Impl : getWeather()");
		String URL = BASE_URL +city+"&appid="+APPID;
		System.out.println(URL);
		 System.out.println("getWeatherResponse");
		 WeatherResponse respString = restTemplate.getForEntity(URL, WeatherResponse.class).getBody();
	        ObjectMapper objectMapper = new ObjectMapper();
	        WeatherResponse resp = null;
	        String strBody = null;
	        System.out.println("WeatherResponse>>>>>>>>>>>>>>"+respString.toString());
	        System.out.println("WeatherResponse>>>>>>>>>>>>>>"+respString.getClouds().toString());
	             
	 
		
		return respString;
		//return this.getWeather(URL);
		
		
	}
	 private WeatherResponse getWeatherResponse(String uri) {
		 System.out.println("getWeatherResponse");
	        ResponseEntity<String> respString = restTemplate.getForEntity(uri, String.class);
	        ObjectMapper objectMapper = new ObjectMapper();
	        WeatherResponse resp = null;
	        String strBody = null;
	        if (respString.getStatusCodeValue() == 200) {
	            strBody = respString.getBody();
	        }
	        try {
	            resp = objectMapper.readValue(strBody, WeatherResponse.class);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        System.out.println(resp);
	        return resp;
	 }
}
