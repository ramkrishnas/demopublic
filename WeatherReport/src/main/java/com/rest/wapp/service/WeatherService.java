package com.rest.wapp.service;

import org.springframework.stereotype.Service;

import com.rest.wapp.pojo.WeatherResponse;

@Service
public interface WeatherService {

	public WeatherResponse getWeather(String city);
}
