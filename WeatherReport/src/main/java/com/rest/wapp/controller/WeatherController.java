package com.rest.wapp.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rest.wapp.pojo.Weather;
import com.rest.wapp.pojo.WeatherResponse;
import com.rest.wapp.service.WeatherService;

@RestController
@RequestMapping("/weather")
public class WeatherController {
	@Autowired
	public WeatherService weatherService;
	@PostMapping("/report")
	public ResponseEntity<?> getReport(@RequestParam("city") String city){
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		//
		WeatherResponse res = weatherService.getWeather(city);
		Weather weather = res.getWeather().get(0);
		
		
		
		return  ResponseEntity.ok().headers(headers)
				.contentType(MediaType.APPLICATION_JSON)//
				.body(weather);
	}
}
